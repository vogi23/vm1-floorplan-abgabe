import gym
from gym import error, spaces, utils
from gym.utils import seeding
import numpy as np
from math import floor
from collections import deque, defaultdict
from shapely.geometry import Point, Polygon
import itertools
from shapely.topology import TopologicalError
import copy
import matplotlib.pyplot as plt


class AbstractLoopWalkerCubeWorld(gym.Env):  
    
    ACTIONS = {
        0 : (-2,0),
        1 : (0,-2),
        2 : (2,0),
        3 : (0,2),
    }
    
    WALL_COST = None
    CORNER_COST = None
    FAIL_COST = None
    
    MAP_UNBUILDABLE = 3
    MAP_POSITION_DEPTH = 0
    MAP_POSITION = 1
    MAP_EMPTY = 0
    MAP_WALL = 1
    MAP_INSIDE_ROOM = 2

    
    def __init__(self):
        self._setup = False
    
    def setup(self, w, h, a, wallCost, cornerCost, failCost):
        if not type(a) is list:
            a = [a]
        self.w = w+4
        self.h = h+4
        self._targetAreas = a
        self.WALL_COST = wallCost
        self.CORNER_COST = cornerCost
        self.FAIL_COST = failCost
        self.action_space = spaces.Discrete(4)
        self._setup = True
        _startstate = self.reset()
        self.observation_space = spaces.Box(low=0, high=1, dtype=np.uint8, shape=_startstate.shape)
    
    def _getStartPoint(self):
        return (self.h//2,self.w//2)
    
    def reset(self):
        assert self._setup, 'call setup(...) first'
        self.accumulatedCosts = 0
        self._currentRoom = 0 # will be incremented to 1 while initializing first room
        self.targetAreas = self._targetAreas.copy()
        self.rooms = [] # list of paths of finished rooms
        self.drawingPaths = [] # list of paths per room
        self.finishedRoomMap = np.zeros((1+len(self.targetAreas),self.h,self.w))
        self.finishedRoomMap[self.MAP_POSITION_DEPTH,0:2,:] = self.MAP_UNBUILDABLE
        self.finishedRoomMap[self.MAP_POSITION_DEPTH,-2:,:] = self.MAP_UNBUILDABLE
        self.finishedRoomMap[self.MAP_POSITION_DEPTH,:,0:2] = self.MAP_UNBUILDABLE
        self.finishedRoomMap[self.MAP_POSITION_DEPTH,:,-2:] = self.MAP_UNBUILDABLE
        self.done = False
        self._initNextRoom(initPoint=self._getStartPoint())
        self._backStepCounter=0.0
        return self._getCurrentState()
    
    def step(self, action, mcts=False):
        assert not self.done, "episode is already done!"
        oldPoint = self._getCurrentPoint()
        point = (oldPoint[0] + self.ACTIONS[action][0], oldPoint[1]+self.ACTIONS[action][1])
        #print(action)
        if self.isOutside(point) or self.isBackstep(point) or self.isInsideFinishedRoom(point):
            return self._getCurrentState(), -self.FAIL_COST, False, None
        
        loop = self.isLoop(point)
        
        if not self.isAlongExistingRoomWall(oldPoint, point):
            self.accumulatedCosts -= self.WALL_COST
        
        if self.isChangeOfDirection(point):
            self.accumulatedCosts -= self.CORNER_COST
        
        self.addWall(point)
        
        done = False
        reward = 0
        if loop:
            reward, done = self.proceedToNextRoom()
        self.done = done
        return self._getCurrentState(), reward, self.done, None
    
    def addWall(self, point):
        self.path.append(point)
        # update current positi
        if len(self.path) > 1:
            self.finishedRoomMap[(self.MAP_POSITION_DEPTH,)+self.path[-2]] = self.MAP_EMPTY
        self.finishedRoomMap[(self.MAP_POSITION_DEPTH,)+point] = self.MAP_POSITION
        # draw wall
        self.finishedRoomMap[(self._getCurrentRoom(),)+point] = self.MAP_WALL
        if len(self.path) > 1:
            oldPoint = self.path[-2]
            intermediatePoint = (oldPoint[0]+(point[0]-oldPoint[0])//2 ,oldPoint[1] + (point[1]-oldPoint[1])//2)
            self.finishedRoomMap[(self._getCurrentRoom(),)+intermediatePoint] = self.MAP_WALL
        
    def proceedToNextRoom(self):
        # update map
        roomReward = self._finishCurrentRoom()
        # setup next room
        return roomReward, self._initNextRoom()

    def _finishCurrentRoom(self):
        roomReward = self.accumulatedCosts + self.evaluateFinishedRoomArea()
        self.accumulatedCosts = 0
        
        vertices = []
        maxX=0
        maxY=0
        minX=self.w-1
        minY=self.h-1
        inLoop = False

        for p in self.path:
            if p == self.path[-1]:
                inLoop=True
            if inLoop:
                vertices.append(p)
            maxX = max([maxX,p[1]])
            minX = min([minX,p[1]])
            maxY = max([maxY,p[0]])
            minY = min([minY,p[0]])

        pol = Polygon(vertices)

        #TODO optimize? by finding first point inside then add neighbours which are not walls recursively
        for y,x in itertools.product(range(minY,maxY+1),range(minX,maxX+1)):
            if self.finishedRoomMap[self._getCurrentRoom(),y,x] == 0 and pol.contains(Point(y,x)):
                self.finishedRoomMap[self._getCurrentRoom(),y,x] = self.MAP_INSIDE_ROOM
        self.rooms.append(vertices)
        self.drawingPaths.append(self.path)
        
       
                                        
        return roomReward

    
    def _initNextRoom(self, initPoint=None):
        if len(self.targetAreas) == 0:
            return True #episode done
        self._currentRoom += 1
        currentPoint = initPoint if not initPoint is None else self._getCurrentPoint()
        self.a = self.targetAreas.pop(0)
        self.path = []
        self.addWall(currentPoint)
        return False

    def isOutside(self, point):
        return point[0] < 2 or point[1] < 2 or point[0] >= self.h-2 or point[1] >= self.w-2
    
    def isBackstep(self, point):
        if len(self.path) < 2:
            return False
        backstep = point == self.path[-2]
        if backstep:
            self._backStepCounter += 1
        return backstep
    
    def isInsideFinishedRoom(self, point):
        return any(self.finishedRoomMap[1:self._getCurrentRoom(), point[0], point[1]] == self.MAP_INSIDE_ROOM)

    def isLoop(self, point):
        return self.finishedRoomMap[(self._getCurrentRoom(),)+point] == self.MAP_WALL
    
    def isChangeOfDirection(self, point):
        if len(self.path) < 2:
            return False
        
        head = self.path[-1]
        prev = self.path[-2]
        lastDir = (head[0]-prev[0], head[1]-prev[1])
        currentDir = (point[0]-head[0], point[1]-head[1])
        return not lastDir == currentDir
    
    def isAlongExistingRoomWall(self,old, new):
        intermediatePoint = (old[0]+(new[0]-old[0])//2 ,old[1] + (new[1]-old[1])//2)
        return any(self.finishedRoomMap[1:self._getCurrentRoom(),intermediatePoint[0],intermediatePoint[1]] == self.MAP_WALL)
    
    def _getCurrentPoint(self):
        return self.path[-1]
    
    def _getCurrentState(self):
        raise NotImplementedError()
                                        
    def _getCurrentRoom(self):
        return self._currentRoom
    
    def evaluateFinishedRoomArea(self):
        return self.a - abs(self.a-self._polyArea())
                                        
    def _polyArea(self):
        head = self.path[-1]
        v = [head]
        closed = False
        for n in reversed(self.path[:-1]):
            v.append(n)
            if n == head:
                closed = True
                break
        
        if closed and len(v) > 2:
            pol = Polygon(v)
            if pol.is_valid:
                aNew = pol.area
                for p in self.rooms:
                    existingPol = Polygon(p)
                    intersection = pol.intersection(existingPol).area
                    aNew -= intersection
                return aNew / 4
        return 0
    
    def render(self, mode='human', close=False):
        canvas = np.full((self.h, self.w), -10)
        i = 0
        rooms = self.drawingPaths + [self.path]
        for p in rooms:
            old = p[0]
            for n in p:
                i += 1
                intermediatePoint = (old[0]+(n[0]-old[0])//2 ,old[1] + (n[1]-old[1])//2)
                old = n
                canvas[intermediatePoint] = i
                i += 1
                canvas[n] = i
            i+=10
            
        canvas[:2,:] = i
        canvas[-2:,:] = i
        canvas[:,:2] = i
        canvas[:,-2:] = i

        return canvas
            
    def seed(self):
        pass
    

class LoopWalkerCubeWorldV1(AbstractLoopWalkerCubeWorld):

    def __init__(self):
        super().__init__()
        self.setup(20, 20, [9], 0.1, 1.6, 3)
    
    def _getCurrentState(self):
        return self.finishedRoomMap
    
    
class LoopWalkerCubeWorldV2(AbstractLoopWalkerCubeWorld):

    def __init__(self):
        super().__init__()
        self.setup(20, 20, [9], 0.1, 0.3, 10)
        
    def step(self, action, mcts=False):
        assert not self.done, "episode is already done!"
        oldPoint = self._getCurrentPoint()
        point = (oldPoint[0] + self.ACTIONS[action][0], oldPoint[1]+self.ACTIONS[action][1])
        if self.isOutside(point) or self.isBackstep(point) or self.isInsideFinishedRoom(point):
            self.done = True
            return self._getCurrentState(), -self.FAIL_COST, True, None # only change since V1
        
        loop = self.isLoop(point)
        
        if not self.isAlongExistingRoomWall(oldPoint, point):
            self.accumulatedCosts -= self.WALL_COST
        
        if self.isChangeOfDirection(point):
            self.accumulatedCosts -= self.CORNER_COST
        
        self.addWall(point)
        
        done = False
        reward = 0
        if loop:
            reward, done = self.proceedToNextRoom()
        self.done = done
        return self._getCurrentState(), reward, self.done, None
    
    def _getCurrentState(self):
        return self.finishedRoomMap
    
    
class LoopWalkerCubeWorldV3(AbstractLoopWalkerCubeWorld):

    def __init__(self):
        super().__init__()
        self.setup(20, 20, [9], 0.1, 0.3, 3)
        
    def _getCurrentState(self):
        return np.moveaxis(self.finishedRoomMap, 0 ,-1)
