import gym
from gym import error, spaces, utils
from gym.utils import seeding
import numpy as np
from math import floor
from collections import deque, defaultdict
from shapely.geometry import Point, Polygon
import itertools
from shapely.topology import TopologicalError
import copy


class AbstractLoopWalkerGridWorld(gym.Env):  
    
    mapStates = {
        0 : 'o',
        1 : 'X'
    }
    
    ACTIONS = {
        0 : (-1,0),
        1 : (0,-1),
        2 : (1,0),
        3 : (0,1),
    }
    
    ACTIONS_LABELS = {
        0: 'UP',
        1: 'LE',
        2: 'DO',
        3: 'RI',
    }
    
    WALL_COST = 0.1
    CORNER_COST = 0.4
    FAIL_COST = 3
    
    MAP_EMPTY = 0
    MAP_WALL = -1
    MAP_WALL_CURRENT = -2
    #MAP_ROOM -> "bigger 0"

    
    def __init__(self):
        self._setup = False
    
    def setup(self, w, h, a, wallCost, cornerCost, failCost):
        if not type(a) is list:
            a = [a]
        self.w = w
        self.h = h
        self._targetAreas = a
        self.WALL_COST = wallCost
        self.CORNER_COST = cornerCost
        self.FAIL_COST = failCost
        self.action_space = spaces.Discrete(4)
        self.observation_space = spaces.Discrete(1) #dummy
        self._setup = True
        self.reset()
    
    def _getStartPoint(self):
        return (self.h//2,self.w//2)
    
    def reset(self):
        assert self._setup, 'call setup(...) first'
        self.targetAreas = self._targetAreas.copy()
        self.rooms = [] # list of paths of finished rooms
        self.drawingPaths = [] # list of paths per room
        self.finishedRoomMap = np.zeros((2,self.h,self.w))
        self.done = False
        self._initNextRoom(initPoint=self._getStartPoint())
        self.takeSnapshot()
        self._areas = [] # for debugging only
        return self._getCurrentState()
    
    def takeSnapshot(self):
        self.snapshot = {
            'targetAreas':copy.deepcopy(self.targetAreas),
            'rooms':copy.deepcopy(self.rooms),
            'drawingPaths':copy.deepcopy(self.drawingPaths),
            'finishedRoomMap':self.finishedRoomMap.copy(),
            'done':copy.deepcopy(self.done),
            'a':copy.deepcopy(self.a),
            '_lastValidArea':copy.deepcopy(self._lastValidArea),
            'lastEvaluatedStateVal':copy.deepcopy(self.lastEvaluatedStateVal),
            'path':copy.deepcopy(self.path),
        }
    
    def restoreLastSnapshot(self):
        self.targetAreas = copy.deepcopy(self.snapshot['targetAreas'])
        self.rooms = copy.deepcopy(self.snapshot['rooms'])
        self.drawingPaths = copy.deepcopy(self.snapshot['drawingPaths'])
        self.finishedRoomMap = self.snapshot['finishedRoomMap'].copy()
        self.done = copy.deepcopy(self.snapshot['done'])
        self.a = copy.deepcopy(self.snapshot['a'])
        self._lastValidArea = copy.deepcopy(self.snapshot['_lastValidArea'])
        self.lastEvaluatedStateVal = copy.deepcopy(self.snapshot['lastEvaluatedStateVal'])
        self.path = copy.deepcopy(self.snapshot['path'])
            
    def step(self, action):
        assert not self.done, "episode is already done!"
        oldPoint = self._getCurrentPoint()
        point = (oldPoint[0] + self.ACTIONS[action][0], oldPoint[1]+self.ACTIONS[action][1])
        if self.isOutside(point):
            return self._getCurrentState(), -self.FAIL_COST, False, None
        if self.isBackstep(point):
            return self._getCurrentState(), -self.FAIL_COST, False, None
        if self.isInsideFinishedRoom(point):
            return self._getCurrentState(), -self.FAIL_COST, False, None

        loop = self.isLoop(point)
        
        reward = 0
        
        
        if self.finishedRoomMap[(1,)+oldPoint] != self.MAP_WALL or self.finishedRoomMap[(1,)+point] != self.MAP_WALL:
            reward -= self.WALL_COST
        
        if self.isChangeOfDirection(point):
            reward -= self.CORNER_COST
        
        self.addWall(point)
        
        oldStateVal = self.lastEvaluatedStateVal
        stateVal = self.evaluateState()
        
        done = False
        if loop:
            reward += (self.a - (abs(self._polyArea()-self.a)))
            done = self.proceedToNextRoom()
            
        reward += stateVal - oldStateVal
        self.lastEvaluatedStateVal = stateVal
        return self._getCurrentState(), reward, done, None
    
    def addWall(self, point):
        self.path.append(point)
        self.finishedRoomMap[0,:,:] = 0
        self.finishedRoomMap[(0,)+point] = 1
        self.finishedRoomMap[(1,)+point] = self.MAP_WALL_CURRENT
        
    def proceedToNextRoom(self):
        # update map
        self._finishCurrentRoom()

        # setup next room
        return self._initNextRoom()

    def _finishCurrentRoom(self):
        vertices = []
        maxX=0
        maxY=0
        minX=self.w-1
        minY=self.h-1
        inLoop = False

        for p in self.path:
            if p == self.path[-1]:
                inLoop=True
            if inLoop:
                vertices.append(p)
            maxX = max([maxX,p[1]])
            minX = min([minX,p[1]])
            maxY = max([maxY,p[0]])
            minY = min([minY,p[0]])
            self.finishedRoomMap[(1,)+p] = self.MAP_WALL

        pol = Polygon(vertices)
        self._areas.append(pol.area) # for debugging only

        #TODO optimize? by finding first point inside then add neighbours which are not walls recursively
        for y,x in itertools.product(range(minY,maxY+1),range(minX,maxX+1)):
            if self.finishedRoomMap[1,y,x] == 0 and pol.contains(Point(y,x)):
                self.finishedRoomMap[1,y,x] = 1 # TODO replace by room number
        self.rooms.append(vertices)
        self.drawingPaths.append(self.path)

    
    def _initNextRoom(self, initPoint=None):
        if len(self.targetAreas) == 0:
            return True #episode done
        currentPoint = initPoint if not initPoint is None else self._getCurrentPoint()
        self.a = self.targetAreas.pop(0)
        self.path = []
        self._lastValidArea = 0
        self.addWall(currentPoint)
        self.lastEvaluatedStateVal = -self.a
        return False

    def isOutside(self, point):
        return point[0] < 0 or point[1] < 0 or point[0] >= self.h or point[1] >= self.w
    
    def isBackstep(self, point):
        if len(self.path) < 2:
            return False
        return point == self.path[-2]
    
    def isInsideFinishedRoom(self, point):
        return self.finishedRoomMap[(1,)+point] > self.MAP_EMPTY

    def isLoop(self, point):
        return self.finishedRoomMap[(1,)+point] == self.MAP_WALL_CURRENT
    
    def isChangeOfDirection(self, point):
        if len(self.path) < 2:
            return False
        
        head = self.path[-1]
        prev = self.path[-2]
        lastDir = (head[0]-prev[0], head[1]-prev[1])
        currentDir = (point[0]-head[0], point[1]-head[1])
        return not lastDir == currentDir
    
    def _getCurrentPoint(self):
        return self.path[-1]
    
    def _getCurrentState(self):
        raise NotImplementedError()
    
    def evaluateState(self):
        return - abs(self.a-self._polyArea()) -((self.path[0][0]-self.path[-1][0])**2 + (self.path[0][1]-self.path[-1][1])**2)**0.5*self.WALL_COST
        
    def _polyArea(self):
        head = self.path[-1]
        v = [head]
        for n in reversed(self.path[:-1]):
            v.append(n)
            if n == head:
                break
        
        aNew = self._lastValidArea
        if(len(v) > 2):
            pol = Polygon(v)
            if pol.is_valid:
                aNew = pol.area
                for p in self.rooms:
                    existingPol = Polygon(p)
                    intersection = pol.intersection(existingPol).area
                    aNew -= intersection
        self._lastValidArea = aNew
        return aNew
    
    def render(self, mode='human', close=False):
        canvas = np.full((self.h, self.w), -10)
        i = 0
        rooms = self.drawingPaths + [self.path]
        for p in rooms:
            for n in p:
                i += 1
                canvas[n] = i
            i+=10

        return canvas
            
    def seed(self):
        pass
    
    
class LoopWalkerGridWorldV1(AbstractLoopWalkerGridWorld):
    
    def __init__(self):
        super().__init__()
        
    def _getCurrentState(self):
        return self.path[-1]
        
class LoopWalkerGridWorldV2(AbstractLoopWalkerGridWorld):
    
    def __init__(self):
        super().__init__()
        
    def _getCurrentState(self):
        return self.finishedRoomMap.tobytes()
    

"""
MCTS Wrapper
"""
class GymEnvMCTSWrapper():
    
    def __init__(self, env):
        self.env = env
        self._reset()
        
        # setup iterable of available actions as integers
        assert isinstance(self.env.action_space, (spaces.Discrete)), "Only 1-dimensional discrete action_space implemented so far"
        self.actions = tuple([a for a in range(self.env.action_space.n)])

    def getPossibleActions(self):
        return self.actions

    def takeAction(self, action):
        self.state, reward, self.done, _ = self.env.step(action)
        self.reward += reward
        return self

    def isTerminal(self):
        if self.done:
            return True
        return False

    def getReward(self):
        assert self.done, "calling getReward on non-terminal state"
        r = self.reward
        self._reset()
        return r
        
    def _reset(self):
        self.reward = 0
        self.done = False
        self.env.restoreLastSnapshot()
    
    

