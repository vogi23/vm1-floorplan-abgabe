import gym
from gym import error, spaces, utils
from gym.utils import seeding
import numpy as np
from math import floor
from collections import deque

class AbstractGrid1DLinearEnv(gym.Env):  
    
    mapStates = {
        0 : 'o',
        1 : 'X'
    }
    
    def __init__(self):
        self._setup = False
    
    def setup(self, n, m, drawingSequenceDequeTemplate=None):
        self.n = n
        self.m = m
        self.drawingSequenceDequeTemplate = drawingSequenceDequeTemplate
        if self.drawingSequenceDequeTemplate is None:
            self.drawingSequenceDequeTemplate = deque(range(self.n))
        self.action_space = spaces.Discrete(2)
        self.observation_space = spaces.Discrete(self.n)
        self._setup = True
    
    def reset(self):
        assert self._setup, 'call setup(...) first'
        self.seed()
        self.map = np.full((self.n,), 0)
        self.drawingSequenceDeque = self.drawingSequenceDequeTemplate.copy()
        self.si = self.drawingSequenceDeque.popleft()
        self.done = False
        return self.si

    def step(self, action):
        assert not self.done, "episode is already done!"
        self.map[self.si] = action
        if len(self.drawingSequenceDeque) == 0:
            self.done = True
            self.si = self.n
        else:
            self.si = self.drawingSequenceDeque.popleft()
        return self.si, self.calcReward(), self.done, None
    
    def calcReward(self):
        raise NotImplementedError()
    
    def isGoal(self):
        goalIdx = np.argwhere(np.convolve(self.map, np.ones((self.m,)), mode='valid') == self.m)
        return True if len(goalIdx) > 0 else False
    
    def render(self, mode='human', close=False):
        g = [self.mapStates[i] for i in self.map]
        return ''.join(g)
        
    def seed(self):
        pass

class Grid1DLinearEnvV1(AbstractGrid1DLinearEnv):
    """Reward of +1 on terminal and goal states only. -1 per unnescessary drawn pixel
    
    """
    
    def calcReward(self):
        reward = 0
        if self.done:
            if self.isGoal():
                reward += self.m + 1
            reward -= np.sum(self.map)
        return reward
    
class Grid1DLinearEnvV2(AbstractGrid1DLinearEnv):
    """(Partial) reward of maxLength/m on terminal and goal states only. -1 per unnescessary drawn pixel
    
    """

    def calcReward(self):
        if self.done:
            maxLength = np.max([0]+[ i+1 for i in range(self.m) if np.max(np.convolve(self.map, np.ones((i+1,)), mode='valid')) == i+1])
            return -np.sum(self.map) + maxLength + maxLength/self.m
        return 0
    
class Grid1DLinearEnvV3(AbstractGrid1DLinearEnv):
    """(Partial) reward of maxLength on terminal and goal states only. -1 per unnescessary drawn pixel
    
    """
    
    def calcReward(self):
        if self.done:
            maxLength = np.max([0]+[ i+1 for i in range(self.m) if np.max(np.convolve(self.map, np.ones((i+1,)), mode='valid')) == i+1])
            return -np.sum(self.map) + maxLength * 2
        return 0
    
class Grid1DLinearEnvV4(AbstractGrid1DLinearEnv):
    """In-episode rewards based on step-by-step evaluation of state
    
    """
    
    def reset(self):
        s = super().reset()
        self.lastStateValue = self.evaluateCurrentState()
        return s
    
    def calcReward(self):
        diff = -self.lastStateValue
        self.lastStateValue = self.evaluateCurrentState(self.map)
        return diff+self.lastStateValue
    
    def evaluateCurrentState(self, map=None, m=None):
        if map is None:
            map = self.map
        if m is None:
            m = self.m
        maxLength = np.max([0]+[ i+1 for i in range(m) if np.max(np.convolve(map, np.ones((i+1,)), mode='valid')) == i+1])
        return -np.sum(map) + maxLength * 2