from gym.envs.registration import register
 
register(id='Grid1DLinear-v1', 
    entry_point='floorplan.envs:Grid1DLinearEnvV1', 
)

register(id='Grid1DLinear-v2', 
    entry_point='floorplan.envs:Grid1DLinearEnvV2', 
)

register(id='Grid1DLinear-v3', 
    entry_point='floorplan.envs:Grid1DLinearEnvV3', 
)

register(id='Grid1DLinear-v4', 
    entry_point='floorplan.envs:Grid1DLinearEnvV4', 
)


register(id='LoopWalkerCubeWorld-v1', 
    entry_point='floorplan.envs:LoopWalkerCubeWorldV1', 
)

register(id='LoopWalkerCubeWorld-v2', 
    entry_point='floorplan.envs:LoopWalkerCubeWorldV2', 
)

register(id='LoopWalkerCubeWorld-v3', 
    entry_point='floorplan.envs:LoopWalkerCubeWorldV3', 
)
