import time
import datetime
import numpy as np
from collections import defaultdict

from tensorboardX import SummaryWriter
from gym.spaces import Discrete 

class QLearningLearner():
    
    def __init__(self, env):
        assert isinstance(env.action_space, Discrete), 'action_space has to be discrete, not '+ str(type(env.action_space))
        self.env = env
        self.reset()
        alpha = 0.1
        gamma = 0.6
        epsilon = 0.1

    def reset(self):
        # iteratively updated expected return per state-action pair: key (state, action), value: (expReturn, nVisists)
        self.q = defaultdict(lambda : [0 for i in range(self.env.action_space.n)])
        self.updatedValues = defaultdict(lambda: 0) # for evaluation of exploration rate only
        self.trainedEpisodes = 0
        self.env.action_space.seed(np.random.randint(10**5, size=1))
        self.env.observation_space.seed(np.random.randint(10**5, size=1))
    
    # get action according to e-greedy policy
    def getAction(self, s, epsilon):
        if np.random.uniform() < epsilon:
            # choose random -> keep exploration
            ai = self.env.action_space.sample()
        else:
            # choose greedy - ties broken arbitrary 
            maxValActionIndices = [i for i, _ in enumerate(self.q[s]) if self.q[s][i] == np.max(self.q[s])]
            ai = np.random.choice(maxValActionIndices)
        return ai

    def runEpisode(self, epsilon=0.1, abortIf=None, learn=False):
        done=False
        s = self.env.reset()
        totalReward = 0
        t = 0
        while(not done):
            a = self.getAction(s, epsilon)
            nextState, reward, done, _ = self.env.step(a)
            if learn:
                self.updateQByReward(s,a,reward,nextState)
            s = nextState
            totalReward += reward
            t += 1
            if not abortIf is None and totalReward < abortIf:
                return totalReward
        return totalReward
                              
    def train(self, epsilon, alpha=0.1, gamma=1, nEpisodes=100000, statsEveryNEpisodes=1000, abortIf=None, nGreedySim=0, reset=True, run=None):
        self.alpha = alpha
        self.gamma = gamma
        rewards = []
        if reset:
            self.reset()
            if run != None:
                # Create a writer for run
                self.writer = SummaryWriter('runs/'+run+'/'+datetime.datetime.today().strftime('%Y_%m_%d__%H_%M_%S'), flush_secs=1)
                              
        for _ in range(nEpisodes):
            if run != None and _ % statsEveryNEpisodes == 0 and not _ == 0:
                self.writer.add_scalar('REWARD/mean', round(np.mean(rewards),2 ), self.trainedEpisodes)
                self.writer.add_scalar('REWARD/std', round(np.std(rewards),2 ), self.trainedEpisodes)
                greedyRewards = []
                if nGreedySim > 0:
                    for ii in range(nGreedySim):
                        totalGreedyReward = self.runEpisode(epsilon=0, abortIf=abortIf)
                        greedyRewards.append(totalGreedyReward)
                    self.writer.add_scalar('GREEDY/mean', np.mean(greedyRewards), self.trainedEpisodes)
                    self.writer.add_scalar('GREEDY/std', np.std(greedyRewards), self.trainedEpisodes)
                rewards = []
            totalReward = self.runEpisode(epsilon=epsilon, abortIf=abortIf, learn=True)
            rewards.append(totalReward)
            self.trainedEpisodes += 1

    def updateQByReward(self, s, a, reward, s1):
        self.updatedValues[(s,a)] += 1
        old_value = self.q[s][a]
        next_max = np.max(self.q[s1])
        new_value = (1 - self.alpha) * old_value + self.alpha * (reward + self.gamma * next_max)
        self.q[s][a] = new_value