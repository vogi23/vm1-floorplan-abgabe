import time
import datetime
import numpy as np
from collections import defaultdict

from tensorboardX import SummaryWriter
from gym.spaces import Discrete 

class OnPolicyFirstVisitMCCLearner():
    
    def __init__(self, env):
        assert isinstance(env.action_space, Discrete), 'action_space has to be discrete, not '+ str(type(env.action_space))
        self.env = env
        self.reset()

    def reset(self):
        # iteratively updated expected return per state-action pair: key (state, action), value: (expReturn, nVisists)
        self.q = defaultdict(lambda : [0 for i in range(self.env.action_space.n)]) 
        self.visits = dict()
        self.updatedValues = defaultdict(lambda: 0) # for evaluation of exploration rate only
        self.trainedEpisodes = 0
        self.env.action_space.seed(np.random.randint(10**5, size=1))
        self.env.observation_space.seed(np.random.randint(10**5, size=1))
    
    # get action according to e-greedy policy
    def getAction(self, s, epsilon):
        if np.random.uniform() < epsilon:
            # choose random -> keep exploration
            ai = self.env.action_space.sample()
        else:
            # choose greedy - ties broken arbitrary 
            maxValActionIndices = [i for i, _ in enumerate(self.q[s]) if self.q[s][i] == np.max(self.q[s])]
            ai = np.random.choice(maxValActionIndices)
        return ai

    def runEpisode(self, epsilon=0.1, render=False):
        done=False
        s = self.env.reset()
        sas = dict() # store state-action pairs and accumulated reward on first-visit
        totalReward = 0
        t = 0
        while(not done):
            a = self.getAction(s, epsilon)
            if (s,a) not in sas: # first-visit MC
                sas[(s,a)] = totalReward # accumulated reward until first-visit
            nextState, reward, done, _ = self.env.step(a)
            s = nextState
            totalReward += reward
            t += 1
            if render > 0 and t % render == 0:
                self.env.render()
        # invert reward as we are interested in accumulated rewards after first-visit
        for sa in sas:
            sas[sa] = totalReward - sas[sa]
        if render != 0 and not (render > 0 and t % render == 0):
            self.env.render()
        return sas, totalReward
                              
    def train(self, epsilon, nEpisodes=100000, statsEveryNEpisodes=1000, nGreedySim=0, reset=True, run=None):                      
        start = time.time()
        rewards = []
        
        if reset:
            self.reset()
            if run != None:
                # Create a writer for run
                self.writer = SummaryWriter('runs/'+run+'/'+datetime.datetime.today().strftime('%Y_%m_%d__%H_%M_%S'), flush_secs=1)
                       
        for _ in range(nEpisodes):
            if run != None and _ % statsEveryNEpisodes == 0 and not _ == 0:
                self.writer.add_scalar('REWARD/mean', round(np.mean(rewards),2 ), self.trainedEpisodes)
                self.writer.add_scalar('REWARD/std', round(np.std(rewards),2 ), self.trainedEpisodes)
                greedyRewards = []
                if nGreedySim > 0:
                    for ii in range(nGreedySim):
                        _, totalGreedyReward = self.runEpisode(epsilon=0)
                        greedyRewards.append(totalGreedyReward)
                    self.writer.add_scalar('GREEDY/mean', np.mean(greedyRewards), self.trainedEpisodes)
                    self.writer.add_scalar('GREEDY/std', np.std(greedyRewards), self.trainedEpisodes)
                start = time.time()
                rewards = []
            stateActionRewards, totalReward = self.runEpisode(epsilon=epsilon)
            rewards.append(totalReward)
            # update q-values for each visited state-action-pair
            for sa in stateActionRewards:
                self.updateQByReward(sa, stateActionRewards[sa])
            self.trainedEpisodes += 1


    def updateQByReward(self, sa, reward):
        self.updatedValues[sa] += 1
        # update average of rewards incrementally
        s = sa[0]
        a = sa[1]
        
        if not sa in self.visits:
            self.visits[sa] = 0
        self.visits[sa] += 1
        self.q[s][a] = self.q[s][a] + (1/self.visits[sa]) * (reward - self.q[s][a])